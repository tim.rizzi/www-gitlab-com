# Local development

## Install prerequisites

1. If you are on macOS, install [Homebrew](https://brew.sh/), which is a
   package manager for macOS that allows you to easily install programs
   and tools through the Terminal. Visit their website for installation
   instructions.
1. Install [rbenv](https://github.com/rbenv/rbenv), a Ruby version
   manager (You can use an alternative such as [RVM](https://rvm.io/),
   or [chruby](https://github.com/postmodern/chruby)). Visit their
   website for installation instructions.
1. **DO NOT** use the system Ruby. Use the ruby version manager to
   install the current [`www-gitlab-com` Ruby
   version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.ruby-version).
   With `rbenv` for example, run the following in the directory for
   `www-gitlab-com`:

   ```sh
   # rbenv will install the ruby specified in .ruby-version
   rbenv install
   ```
1. Install bundler by running the following in the directory for `www-gitlab-com`:

   ```sh
   gem install bundler
   ```


## Run middleman

```sh
bundle install

bundle exec middleman
```

Once the Middleman server is running, you can visit
[http://localhost:4567](http://localhost:4567) in your browser to see a live,
local preview of the site. Any changes to files in the `source` directory will
be detected automatically, and your browser will even reload the page if necessary.

PDF files are not available in development mode. See below for more information.

See the [Middleman docs](https://middlemanapp.com/basics/development_cycle/) for
more information.

## Enable livereloading

When running middleman with the livereload option enabled, it watches your
repo for changes and reloads the site automatically.

Livereload can result to [slow server response times][gh-livereload], so it is
disabled by default. That means you need to manually refresh the webpage if you
make any changes to the source files. To enable it, just set the environment
variable `ENABLE_LIVERELOAD=1` before running middleman:

```
ENABLE_LIVERELOAD=1 bundle exec middleman
```

You can verify that it's enabled from the following line:

```
== LiveReload accepting connections from ws://192.168.0.12:35729
```

To permanently have livereload enabled without typing the environment variable,
just export its value in your shell's configuration file:

```
# Open your rc file (replace editor with vim, emacs, nano, atom, etc.)
editor ~/.bashrc

# Export the livereload variable
export ENABLE_LIVERELOAD=1
```

>**Note:**
You need to logout and login in order for the changes to take effect. To
temporarily use the changes, run `source ~/.bashrc`.

[gh-livereload]: https://github.com/middleman/middleman-livereload/issues/60

## Preview `/direction/`

The [direction](https://about.gitlab.com/direction/) page is generated
by a mix of [markdown text](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/template.html.md.erb)
and content generated automatically by fetching data from different projects.

For the sake of build speed while developing the website, the direction page is
only built from the `master` branch of the website project. This means that locally,
it returns a 404, as well as for Review Apps.

A workaround for previewing it locally is enabling the `master` environment
when building the site with middleman:

1. Open a terminal window on `www` and build the website:

    ```shell
    CI_BUILD_REF_NAME=master PRIVATE_TOKEN=your_access_token middleman build
    ```

    Your [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    can be generated in your GitLab.com's profile **Settings > Access Tokens**

1. Wait until it builds (it takes about 10 min)
1. The website will be built in a folder called `/public/` in the root dir
1. Install a static server `gem install adsf`
1. Open the `/public/` folder in another terminal and run `adsf` to start the server
1. Preview at port 3000: `http://localhost:3000/direction/`

Also, for the sake of speed, the issue info is cached for 24h in a local file cache 
at `./tmp/cache/direction/`. If you want to requery the GitLab servers for updated 
issue information, you can delete that cache locally (if you're using a local 
development environment), or run a GitLab.com pipeline with the key 
`CLEAR_DIRECTION_CACHE` set to clear it on the remote runner. 

Notes:

- If previewing something else on port 3000, change the port to something else,
e.g., `adsf -p 3001`, to preview at `http://localhost:3001/direction/`
- You'll have to build the site it again to preview a new change

## Troubleshooting

1. Do check that the Ruby version matches the [expected Ruby
   version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.ruby-version).
   Run the following in the directory for `www-gitlab-com`:

   ```sh
   ruby --version
   ```
