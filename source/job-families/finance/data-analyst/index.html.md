---
layout: job_family_page
title: "Data Analyst"
---

## Responsibilities

* Expand our data warehouse with clean data, ready for analysis
* Understand and document the full lifecycle of data from numerous sources and how to model it for easy analysis
* Build reports and dashboards to help teams identify opportunities and explain trends across data sources
* Follow and improve our processes for maintaining high quality data and reporting
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Collaborate with other functions to create useful analyses and democratize insights across the company
* Build upon and document our common data framework so that all data can be connected and analyzed
* This position reports to the Manager, Data & Analytics



## Requirements

* 2+ years experience in an analytics role
* Deep understanding of SQL and relational databases (we use Snowflake)
* Hands on experience working with Python and SQL to generate business insights and drive better organizational decision making
* Experience building reports and dashboards in a data visualization tool (we use Looker)
* Passionate about data, analytics and automation. Experience cleaning and modeling large quantities of raw, disorganized data (we use dbt)
* Experience with a variety of data sources. Our data includes Salesforce, Zuora, Zendesk, Marketo, Snowplow and many others
* Share and work in accordance with our values
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)


## Senior Requirements
All of the responsibilities of a Data Analyst, plus:
* Advocate for improvements to analysis quality, security, and performance that have particular impact across your team and the organization
* Solve technical problems of high scope and complexity
* Exert influence on the overall objectives and long-range goals of your team
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions
* Provide mentorship for Junior and Intermediate Analysts on your team to help them grow in their technical responsibilities and remove blockers to their autonomy
* Confidently ship moderately sized analyses and transformation models with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Build close relationships with other functional teams to truly democratize data understanding and access


## Specialties

### Growth
* Support the product management function in driving product growth, reducing churn, increasing user engagement by analyzing data and discovering insights
* Focus on product-specific data - usage ping, SaaS DB, Snowplow events
* Priorities will be set by a Growth Product Manager but will collaborate with and report into the Data Team


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to fill out a short questionnaire.
* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Analyst
* Next, candidates will be invited to schedule a third interview with our Dir. of Business Operations
* Next, if applying for the Growth specialty, candidates will be invited to schedule a fourth interview with our Dir. of Product, Dev
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
