---
layout: markdown_page
title: "Leaders at Contribute"
---

- TOC
{:toc}

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/39chczWRKws?start=1756" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<small>
For the context of this video, please see the [summit challenges in the Greece recap](https://about.gitlab.com/2017/10/25/gitlab-summit-greece-recap/#summit-challenges)</small>

[Company leaders](/handbook/leadership/) who attend Gitlab Contribute should take the opportunity to engage with GitLabbers across the organization and should be mindful of silos and favoritism as they observe team dynamics.

### Be conscious

Leaders should consider themselves "hosts" of the event, which means that, in addition to facilitating team building and interaction, they should be aware of potential issues that could arise during the event related to the behavior, health, and safety guidelines below. These include:
- Harassment, sexual or otherwise
- Excessive drinking and/or drug use
- Fighting and physical altercations
- Accidents (e.g. trips, falls, motor accidents, etc.)
- Inappropriate dress (business/work event)
- Various mental states and moods (anxiety, depression, etc); [neurodiversity](https://about.gitlab.com/handbook/values/#diversity)

### What should managers do?
- Protect and reduce risk to all team members and to the company
- Check on your team throughout summit
- Err on the side of caution and over-communication by contacting People Ops immediately if there is an issue or if you think there may be a problem/concern

**Who to contact?**
- Barbie Brewer and Julie Armendariz - onsite at Summit in South Africa
- Jessica Mitchell - available via Slack
- People Operations Team - can be reached via Slack and people ops email address

## Behavior

* Attending the summit is optional but recommended. Most people report it is great to get to know each-other better and the schedule is fun, about 90% of the team is able to attend.
* The executive team is required to attend for the full duration of the summit.
* Wear your name-tag when you're outside your room, including during excursions, meals, and on departure day.
* Try to join different people every time we sit down for a meal
* Try to form a personal bond with team members of other teams.
* The summit is great for informal meetings and brainstorming, like User Generated Content discussions. People already know their team, so try to make UGC sessions cross functional.
* Don't plan meetings and 1-1's with your own team at the summit, we already do these when we're not at the summit. It is OK to organize one dinner with the team.
* Prior to the summit, ensure to communicate to any external stakeholders (i.e. candidates, customers, vendors, etc) that response time may be less reliable, as you will be out of the office and will not have as much access to email and calls.
* Respect the laws and customs of the location we are visiting.

### Extraverts and Introverts
Remember that you and your co-workers may have different personality types in terms of how you interact with others in large social situations. Consider the differences between extraverts and introverts:

**Extraverts**

- Recharge by being Social
- Enjoy group conversations
- Speak more
- Make decisions quickly
- Love getting attention
- Speak up in meetings

**Introverts**

- Recharge by spending time alone
- Enjoy one-on-one conversations
- Listen more
- Reflect before making decisions
- Are not interested in getting attention
- Shares ideas when prompted

**Ambiverts**

- A combination of both introverts and extraverts

References:
[Are Extraverts Happier Than Introverts? Psychology Today](https://www.psychologytoday.com/blog/thrive/201205/are-extroverts-happier-introverts)
[Are You an Extravert, Introvert, or Ambivert?](https://www.psychologytoday.com/us/blog/cutting-edge-leadership/201711/are-you-extravert-introvert-or-ambivert)

## Health and safety
{: #health-and-safety}

* Look after your self during the summit and avoid summit burnout. It can be an exciting time with lots of new people to meet and things going on you want to take part in. Remember to take down-time if you need it to recuperate during the week rather than trying to [burn the candle at both ends](https://dictionary.cambridge.org/dictionary/english/burn-the-candle-at-both-ends) and risking exhaustion.
* Every year about a third of us have some kind of flu after the summit, so please take infection prevention seriously.
* Use fist-bumps instead of handshaking to [reduce the number of people who get sick](https://www.health.harvard.edu/blog/fist-bump-better-handshake-cleanliness-201407297305).
* Use hand sanitizer after getting your food and before eating. [Shared buffet utensils spread disease.](http://www.cruisereport.com/crBlogDetail.aspx?id=3683) We will try to provide hand sanitizer.
* If you are sick please wear a [surgical mask](https://www.amazon.com/Maryger-Disposable-Procedure-Surgical-Counts/dp/B06XVMT3ZH/ref=sr_1_1_sspa?s=hpc&ie=UTF8&qid=1509481716&sr=1-1-spons&keywords=surgical+mask&psc=1) which [reduces the spreading by up to 80%](https://www.healthline.com/health/cold-flu/mask). We'll try to provide them.
* Remember our [values](/handbook/values/) and specifically the [permission to play](/handbook/values/#permission-to-play) behavior
* Be respectful of other hotel guests (e.g. don't talk on your floor when returning
to your room at night & keep your volume down at restaurants/bars).
* Utilize the resources available to understand the safety and crime considerations in the location we are visiting. Examples are the [UK's Foreign Travel Site](https://www.gov.uk/foreign-travel-advice) and the [U.S. State Department](https://travel.state.gov/content/passports/en/country.html). If you are alarmed by what you are reading, please feel free to reach out to People Ops Team with your concerns. We also advise reviewing the data for countries you feel are safe. You may find that even the safest countries have warnings on crime and safety. Staying with a group and away from known dangerous areas is the most basic way of avoiding problems.

## Cultural Impacts and Differences
It is important to recognize that people from different cultures have different ways of saying things, different body language, different ways of dressing/attire and even different ways of looking at things. You can review examples of typical cultural differences on the [Center of Intercultural Competence](http://www.cicb.net/en/home/examples).

Summit attendees should also remember:
- We have various generations and 40+ countries/cultures coming together for 5 straight days, versus our normal routine of interacting remotely.
- We are guests of this country, of the hotel, and of the sites we will visit - it is our privilege to be there, and we need to be ambassadors of GitLab and lead by example the whole time according to [our values](https://about.gitlab.com/handbook/values/).
- Be sensitive to political, religious and other potentially divisive conversations

### Lost in Translation
Translation tends to sound easier than it is. People often think that it is just a matter of replacing each source word with a corresponding translated word, and then you are done. Assume best intent from your fellow team members and use it as an opportunity start a new dialogue. For more information you can review the following articles on [LTC Language Solutions](https://ltclanguagesolutions.com/blog/lost-in-translation-translating-cultural-context/)