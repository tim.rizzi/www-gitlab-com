---
layout: markdown_page
title: "Category Vision - Release Governance"
---

- TOC
{:toc}

## Release Governance

Release Governance includes features such as deploy-time security controls to ensure only trusted container images are deployed on Kubernetes Engine, and more broadly includes all the assurances and evidence collection that are necessary for you to trust the changes you're delivering. For example, having a strong integration with CI/CD system to ensure artifact chain of custody and traceability all the way to commit, assurance of test completion, quality gates, auditing, and any other governance requirements are enforced in the release.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20governance&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

## What's Next & Why

Up next is binary authorization, a key part of [&762](https://gitlab.com/groups/gitlab-org/-/epics/762). This will be delivered via three components:

- [gitlab-ee#7268](https://gitlab.com/gitlab-org/gitlab-ee/issues/7268): Binary authorization pipeline
- [gitlab-ee#7840](https://gitlab.com/gitlab-org/gitlab-ee/issues/7840): Make enabling binauthz in GKE easy
- [gitlab-ee#7839](https://gitlab.com/gitlab-org/gitlab-ee/issues/7839): Technical article explaining how to get started

## Competitive Landscape

A key capability of products which securely manage releases is to collect evidence associated with releases in a secure way. [gitlab-ce#56030](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030) introduces a new kind of entity that is part of a release, which contains various kinds of evidence (test results, security scans, etc.) that were collected as part of a release generation.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more legacy style deployment workflows so changes like [gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187) (which adds manual approval jobs/gates to the GitLab pipeline) will help us perform better for analysts and the kinds of customers who are approaching release management from a more top-down perspective.

Similarly, integrations with technologies like ServiceNow ([gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373)) will help GitLab fit in better with larger enterprise governance workflows.

## Top Customer Success/Sales Issue(s)

The CS team sees requests for integration with ServiceNow for change management built in to CD pipelines, as per [gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373).

## Top Customer Issue(s)

[gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187) is the most upvoted item and adds an explicit approval job to handle approvals in release workflows.

## Top Internal Customer Issue(s)

[gitlab-ce#21583](https://gitlab.com/gitlab-org/gitlab-ce/issues/21583) has been requested by the delivery team / @marin to allow for more secure, locked down access to production-type environments instead of relying on more broad project permissions.

Our compliance team also is documenting their [scope](https://about.gitlab.com/handbook/engineering/security/sec-controls.html), and we can investigate further options on how to provide solutions for the areas that relate to release governance.

## Top Vision Item(s)

[gitlab-ee#8945](https://gitlab.com/gitlab-org/gitlab-ee/issues/8945) is a large issue that introduces more controls on the pipeline itself, ensuring that assets are handed off between stages in a secure way, that compliance evidence is collected and authenticated with binaries, and overall that the path to production is more secure and auditable than it is today. Very much related is binary authorization ([gitlab-ee#7268](https://gitlab.com/gitlab-org/gitlab-ee/issues/7268)) which provides a secure means to validate deployable containers.

Blackout periods ( [gitlab-ce#51738](https://gitlab.com/gitlab-org/gitlab-ce/issues/51738)) will help compliance teams enforce periods where production needs to remain stable/not change.
