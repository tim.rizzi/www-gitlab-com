## Data Quality Process

The goal of the DQP is to ensure that changes in metrics are documented and communicated properly. Data are dynamic and the methods use to perform analytics are constantly changing and improving. If a change is going to impact certain [operating metrics](https://about.gitlab.com/handbook/finance/operating-metrics/) we want to be sure we document what changed and why it changed. We also want to communicate it out as quickly and effectively as possible.


### Metrics

The following metrics require notifications per the DQP:


#### Tier 1

* Annual Recurring Revenue (ARR)
* Net and Gross Retention
* Customer Counts


### Process

When an upcoming change is identified, an issue will be opened in the Business Operations project using the [Data Quality Process issue template](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/new?issuable_template=Data%20Quality%20Process).

All code changes happen in the two primary repos for the Data Team: [Analytics](https://gitlab.com/meltano/analytics/) and [Looker](https://gitlab.com/meltano/looker/). The relevant issues there will be cross-linked to the issue in the Business Operations project. 

The issue template will update and evolve over time, but at a minimum there will be a before and after of the metric documented, links to relevant issues and code changes, and documentation of why the change happened. 

Tier 1 metrics require notification all the way to the board. CEO notification will happen once the change has been implemented and documented. Board notification will be done at the next board meeting unless the change is so severe as to warrant immediate notification via an investor update.

As more metrics are added to the DQP, lower severity tiers will be added.