---
layout: markdown_page
title: "Board of Directors and Governance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Board Meeting Process

1. The CFO is responsible for scheduling the meeting, preparing the agenda and recording minutes.
1. Collaborate on public webpages such as [/strategy](/company/strategy/) as much as possible.
1. Financial information and other non public items go into a shared Google Sheet and/or Google Presentation.
1. The CFO will send a reminder to those who are requested to prepare materials two weeks in advance of the meeting.
1. Final draft presentations are due one week prior to the meeting.
1. Board materials are distributed the friday before the meeting.
1. There will be two deep dives on the agenda for each board meeting:
    - 30 minutes alotted to a single functional area (i.e. sales, marketing, engineering, product, g&a, etc) on a rotation basis
    - 30 minutes alotted to a topic(s) of strategic or operational importance to the Company.
1. Discussion points are clearly marked, each get a time allotment, five minutes unless otherwise approved.
1. Board members are assumed to have studied the materials.
1. For now the whole executive team is present during the meeting.
1. There is a closed session at the end to cover administrative items and board only discussion.
1. No presentation during the meeting, only discussion items and conversation about unclear items.
1. Follow up with updated materials.

## Board Committees
1. Audit Committee
 - Members: Larry Augustin, Villi Iltchev, Bruce Armstrong
1. Compensation Committee
 - Members: Villi Iltchev, Bruce Armstrong, Matthew Jacobson
1. Nominations and Governance
 - Members: Matt Mullenweg, Matthew Jacobson, Sid Sijbrandij

## Schedule
1. Board of Directors meetings are held quarterly and can be attended either in person or by videoconference.
1. Meetings are scheduled as close to the fourth thursday following the end of the quarter, depending on availability of the directors.
1. The 2019 schedule of board meetings is as follows:


EA shall ensure that there is one invite for all attendees that includes the following:
Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
Zoom Link
Agenda (the agenda should also include the zoom link at the top)

Make sure to:
Determine which participants will be attending remotely and in-person to ensure there is enough room.
Test Zoom setup at least 1 hour before and at most 2 days before
Ensure remote participants feel invited and welcomed

## Quarterly Q&A with the Board
We will have one board member per quarter conduct an AMA session with the GitLab team.

## References

1. [AVC post](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/)
1. [AVC comment](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/#comment-2489615046)
1. [Techcrunch article](http://techcrunch.com/2016/02/01/1270130/)


## Audit Committee Charter (adopted 2018-04-26)

1.	Purpose. The purpose of the Audit Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board’s oversight of:
    - The integrity of the Company’s financial statements;
    - The performance, qualifications and independence of the Company’s registered public accounting firm (the “external auditors”);
    - The performance of the Company’s internal financial, accounting and reporting controls and other processes.
1.	Structure and Membership
    - Members. the Audit Committee shall consist of at least two members of the Board, each of whom shall be independent.
    - Financial Literacy. Each member of the Audit Committee must be financially literate, as such qualification is interpreted by the Board in its business judgment, or must become financially literate within a reasonable period of time after his or her appointment to the Audit Committee.
    - Chair. Unless the Board elects a Chair of the Audit Committee, the Audit Committee shall elect a Chair by majority vote.
    - Selection and Removal. Members of the Audit Committee shall be appointed by the Board.
1.	Authority and Responsibilities
    - General. The Audit Committee shall discharge its responsibilities, and shall assess the information provided by the Company’s management and the external auditors, in accordance with its business judgment. Management is responsible for the preparation, presentation, and integrity of the Company’s financial statements and for the appropriateness of the accounting principles and reporting policies that are used by the Company. The external auditors are responsible for auditing the Company’s financial statements. The authority and responsibilities set forth in this Charter do not reflect or create any duty or obligation of the Audit Committee to plan or conduct any audit, to determine or certify that the Company’s financial statements are complete, accurate, fairly presented, or in accordance with generally accepted accounting principles or applicable law, or to guarantee the external auditors’ reports.
    - Oversight of Integrity of Financial Statements
    - Review and Discussion. The Audit Committee shall meet to review and discuss with the Company’s management and external auditors the Company’s audited financial statements.
    - Related-Person Transactions. The Audit Committee shall review related-person transactions under the Company’s Related Person Transaction Policy and applicable accounting standards on an ongoing basis and such transactions shall be approved by the Audit Committee.
    - Oversight of Performance, Qualification and Independence of External Auditors
1.	Selection. The Audit Committee shall be responsible for appointing, evaluating and, when necessary, terminating the engagement of the external auditors. The Audit Committee may, in its discretion, seek stockholder ratification of the external auditors it appoints.
1.	Independence. The Audit Committee shall assist the Board in its assessment of the independence of the external auditors. In connection with this assessment, the Audit Committee shall, at least annually, obtain and review a report from the external auditors describing relationships between the external auditors and the Company, including the disclosures required by the applicable requirements of the Public Company Accounting Oversight Board regarding the external auditors’ independence. The Audit Committee shall actively engage in dialogue with the external auditors concerning any disclosed relationships or services that might impact the objectivity and independence of the external auditors.
1.	Compensation. The Audit Committee shall be directly responsible for setting the compensation of the external auditors. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of the external auditors established by the Audit Committee.
1.	Oversight. The external auditors shall report directly to the Audit Committee and the Audit Committee shall be directly responsible for overseeing the work of the external auditors, including resolution of disagreements between Company management and the external auditors regarding financial reporting.
1.	Procedures and Administration
    - Meetings. The Audit Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Audit Committee may also act by unanimous written consent in lieu of a meeting. The Audit Committee shall periodically meet separately with: (i) the external auditors and (ii) Company management. The Audit Committee shall keep minutes of its meetings and provide those to the Board of Directors.
1. 	Independent Advisors. The Audit Committee shall have the authority, without further action by the Board, to engage and determine funding for such independent legal, accounting and other advisors as it deems necessary or appropriate to carry out its responsibilities. Such independent advisors may be the regular advisors to the Company. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of such advisors as established by the Audit Committee.
1.	Investigations. The Audit Committee shall have the authority to conduct or authorize investigations into any matter within the scope of its responsibilities, as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Audit Committee or any advisors engaged by the Audit Committee.
1.	Additional Powers. The Audit Committee shall have such other duties as may be delegated. 

## Compensation Committee Charter (adopted 2018-04-26)
1.	Purpose
The purpose of the Compensation Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board in the performance of its responsibilities relating to the Company’s compensation programs in general and specifically, but not limited to, its’ executive officers.

1.	Structure and Membership
    - Number. The Compensation Committee shall consist of at least two members of the Board.
    - Independence. At least two members of the Compensation Committee shall not have management responsibilities.
    - Chair. Unless the Board elects a Chair of the Compensation Committee, the Compensation Committee shall elect a Chair by majority vote.
    - Compensation. The compensation of Compensation Committee members shall be as determined by the Board.
    - Selection and Removal. Members of the Compensation Committee shall be appointed by the Board. The Board may remove members of the Compensation Committee from such committee, with or without cause, at any time that it determines to do so.

1.	Authority and Responsibilities
    - General. The Compensation Committee shall perform its responsibilities, and shall assess the information provided by the Company's management, in accordance with its business judgment.
    - Compensation Matters
    - CEO Compensation and Performance. The Compensation Committee shall annually review and approve corporate goals and objectives relevant to the compensation of the Company’s Chief Executive Officer (the “CEO”), evaluate the CEO’s performance in light of those goals and objectives, and, either as a committee or together with the other independent directors (as directed from time to time by the Board), determine and approve the CEO’s compensation based on this evaluation.
    - Executive Officer Compensation. The Compensation Committee shall review and approve, or recommend for approval by the Board, executive officer (including the CEO) compensation, including salary, bonus and incentive compensation levels; deferred compensation; executive perquisites; equity compensation (including awards to induce employment); severance arrangements;
change-in-control benefits and other forms of executive officer compensation. The Compensation Committee shall meet without the presence of executive officers when approving or deliberating on CEO compensation but may, in its discretion, invite the CEO to be present during approval of, or deliberations with respect to, other executive officer compensation.

1.  Plan Recommendations and Approvals. The Compensation Committee shall periodically review and make recommendations to the Board with respect to incentive-compensation plans and equity-based plans that are subject to approval by the Board.

1.  Director Compensation. The Compensation Committee shall periodically review and make recommendations to the Board of Directors with respect to director compensation.

1.  Additional Powers. The Compensation Committee shall take such other action with respect to compensation matters as may be delegated from time to time by the Board.

1.  Procedures and Administration
    - Meetings. The Compensation Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Compensation Committee may also act by unanimous written consent in lieu of a meeting. The Compensation Committee shall keep such records of its meetings and furnish the minutes of such meetings to the Board of Directors.
    - Charter. The Compensation Committee shall periodically review and reassess the adequacy of this Charter and recommend any proposed changes to the Board for approval.
    - Compensation Consultants, Legal Counsel and Other Advisors. The Compensation Committee may, in its sole discretion, retain, terminate or obtain the advice of compensation consultants, legal counsel or other advisors. The Compensation Committee shall be directly responsible for the appointment, compensation and oversight of the work of any compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee is empowered, without further action by the Board, to cause the Company to pay the compensation, as determined by the Compensation Committee, of any
compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee may select, or receive advice from, a compensation consultant, legal counsel or other advisor, only after taking into consideration, as applicable, all factors relevant to that person’s independence from management.

1.	Investigations. The Compensation Committee shall have the authority to conduct or authorize investigations into any matters within the scope of its responsibilities as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Compensation Committee or any advisors engaged by the Compensation Committee.

