---
layout: markdown_page
title: "E-group KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

Every executive at GitLab has Key Performance Indicators (KPIs).
This page has the goals and links to the definitions.
The goals are merged by the CEO.
The definitions should be in the relevant part of the handbook.
In the definition it should mention what the canonical source is for this indicator.

## Public

In the doc 'GitLab Metrics' at IPO are the KPIs that we may share publicly.

## CEO

CEO goals are duplicates of goals of the reports.
The CEO goals are the most important indicators of company performance.

1. [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1
1. [ARR](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) YoY > 190%
1. Pipe generated vs. plan > 1
1. Wider community contributions per release
1. [CAC / LTV](https://about.gitlab.com/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4
1. Average NPS
1. Hires vs. plan > 0.9
1. Monthly employee turnover
1. New hire average score
1. Merge Requests per release per developer
1. Uptime GitLab.com
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. [Support CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. Runway > 12 months
1. MAAUI (monthly active users of UI)[^1] 
[^1]: MAUUI is a [Meltano](https://meltano.com/) specific KPI and an exception to the CEO goals. MAUUI is not part of the GitLab Executive Team KPIs.

## CRO

[Looker 45](https://gitlab.looker.com/dashboards/45)

1. [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2
1. [TCV](https://about.gitlab.com/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1
1. [ARR](https://about.gitlab.com/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190%
1. Win rate > 30%
1. % of ramped reps at or above quota > 0.7
1. [Net Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 2
1. [Gross Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 0.9
1. Rep IACV per comp > 5
1. [ProServe](/handbook/customer-success/implementation-engineering/offerings/) revenue vs. cost > 1.1
1. Services attach rate for strategic > 0.8
1. Self-serve sales ratio > 0.3
1. Licensed users
1. ARPU
1. New strategic accounts
1. IACV per Rep > $1.0M
1. New hire location factor < TBD

## CMO

[Looker 51](https://gitlab.looker.com/dashboards/51)

1. Pipe generated vs. plan > 1
1. Pipe-to-spend > 5
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2
1. SCLAU
1. [CAC / LTV ratio](https://about.gitlab.com/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4
1. Twitter mentions
1. Sessions on our marketing site
1. New users
1. Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. [Social response time](/handbook/marketing/community-relations/community-advocacy/#community-response-channels)
1. Participants at meetups with GitLab presentation
1. GitLab presentations given
1. Wider community contributions per release
1. Monthly Active Contributors from the wider community
1. New hire location factor < X

## CCO

[Looker 44](https://gitlab.looker.com/dashboards/44)

1. Hires vs. plan > 0.9
1. Apply to hire days < 30
1. No offer NPS > [4.1](https://stripe.com/atlas/guides/scaling-eng)
1. Offer acceptance rate > 0.9
1. Average NPS
1. Average location factor
1. New hire location factor < TBD
1. Monthly employee turnover
1. YTD employee turnover
1. Candidates per vacancy
1. Percentage of vacancies with active sourcing
1. New hire average score
1. Onboarding NPS
1. Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention
1. PeopleOps cost per employee
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 

## CFO

[Looker 43](https://gitlab.looker.com/dashboards/43)

1. IACV per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0
1. [Magic number](https://about.gitlab.com/handbook/finance/operating-metrics/#magic-number) > 1.1
1. Gross margin > 0.9
1. [Average days of sales outstanding](https://about.gitlab.com/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45
1. Average days to close < 10
1. Runway > 12 months
1. New hire location factor < 0.7
1. ARR by annual cohort
1. Reasons for churn
1. Reasons for net expansion
1. Refunds processed as % of orders


## VP of Product

[Looker 69](https://gitlab.looker.com/dashboards/75) only has MAU and SMAU.

1. SMAU 
1. MAU
1. Sessions on release post
1. Installation churn
1. User churn
1. New hire location factor < TBD

## VP of Engineering

[Looker 46](https://gitlab.looker.com/dashboards/46)

1. Merge Requests per release per engineer in product development > 10
1. Uptime GitLab.com > 99.95%
1. Performance GitLab.com
1. Support SLA
1. Support [CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. Support cost vs. recurring revenue
1. Days to fix S1 security issues
1. Days to fix S2 security issues
1. Days to fix S3 security issues
1. GitLab.com infrastructure cost per MAU
1. ARR per support rep > $1.175M
1. New hire location factor < 0.5
1. Public Cloud Spend

## VP of Alliances

[Looker 53](https://gitlab.looker.com/dashboards/53) is broken.

1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Active installations per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. Downloads: Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source
1. Acquistion velocity: [Acquire 3 teams per quarter](/handbook/alliances/acquisition-offer/) for less than $2m in total.
1. Acquistion success: 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquistion.
