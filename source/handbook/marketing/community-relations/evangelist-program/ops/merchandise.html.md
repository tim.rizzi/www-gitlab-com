---
layout: markdown_page
title: "Merchandise operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Community Relations manages the merchandise store. This includes:
- adding and removing items to and from the store,
- fulfilling orders,
- maintaining inventory levels and
- responding to store support requests

We are currently using the following vendors:
- Storefront:
  - Shopify
  - Amazon (trial)
- Inventory:
  - Sendoso
  - Stickermule

## Access data

- URL: https://shop.gitlab.com
- Login: see 1Password secure note for details

## Operations

### Storefront vendors

#### Shopify

##### Access data

- URL: https://www.shopify.com
- Login: see 1Password secure note for details

##### Adding items

1. Gather item inventory data - contact the product's vendor
1. Log in to Shopify
1. Open the products page
   - Click the Add Product button
   - Fill out information about the item
   - If you don't have the information for the description, please ask/search for it and be careful - the info could be sensitive
   - Image is important, contact the product vendor for the high-rez photo.
   - Fill out the price for the item
   - Select "Shopify tracks this product's inventory"
   - Enter the weight of the product if that info is available.
   - Before saving the product, please check search engine listing preview

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.shopify.com/en/manual/products/add-update-products)
{: .alert .alert-info}

##### Removing items

1. Log in to Shopify
1. Open the Products page
   - Click on the product you want to remove
   - Scroll to the bottom of the page where you can find the delete button

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [offical guide](http://shopifynation.com/shopify-tutorials/delete-products-variants-shopify/)
{: .alert .alert-info}

### Amazon

#### Access data

- URL: https://sellercentral.amazon.com/
- Login: Use Community Relations 1Password credential from the marketing vault

## Inventory vendors

### Sendoso

#### Access data

- URL: https://sendoso.com
- Login: Use Community Relations 1Password credential from the marketing vault.

### Stickermule

#### Access data

- URL: https://www.stickermule.com
- Login: see 1Password secure note for details
